import React from 'react';
import Splash from './Container/Splash';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import Login from '../src/Container/Login';
import ForgotPassword from '../src/Container/ForgotPassword';
import Notifications from '../src/Container/Notifications';
import ProfileList from '../src/Container/ProfileList';
import profileContainer from '../src/Container/profileContainer';
import HomeContainer from './Container/HomeContainer';
import newsFeedContainer from './Container/newsFeedContainer';
import StudentRegister from './Container/StudentRegister';
import ParentRegister from './Container/ParentRegister';
import ChangePassword from './Container/ChangePassword';
import NotificationSettingContainer from './Container/NotificationSettingContainer';
import SocialMediaContainer from './Container/SocialMediaContainer';
import SchoolidContainer from './Container/SchoolidContainer';
import MyCollection from './Container/MyCollection';
import {createDrawerNavigator} from '@react-navigation/drawer';
import CustomDrawer from '../src/Container/CustomDrawer';
import TableContainer from '../src/Container/TableContainer';
import PublicProfile from '../src/Container/PublicProfile';
import SingleYearBook from '../src/Container/SingleYearBook';
import ContentsContainer from '../src/Container/ContentsContainer';
import PrivacyContainer from './Container/PrivacyContainer';
import TermsContainer from './Container/TermsContainer';

const BASE_STACK = createNativeStackNavigator();
const Drawer = createDrawerNavigator();

const homeDrawer = () => {
  return (
    <Drawer.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName={'Home'}
      drawerContent={props => <CustomDrawer {...props} />}>
      <Drawer.Screen name="Home" component={HomeContainer} />
      <Drawer.Screen name="Notifications" component={Notifications} />
      <Drawer.Screen name="profileList" component={ProfileList} />
      <Drawer.Screen name="profile" component={profileContainer} />
      <Drawer.Screen name="news" component={newsFeedContainer} />
      <Drawer.Screen name="changePass" component={ChangePassword} />
      <Drawer.Screen name="SingleYearBook" component={SingleYearBook} />
      <Drawer.Screen
        name="notificationSetting"
        component={NotificationSettingContainer}
      />
      <Drawer.Screen name="schoolId" component={SchoolidContainer} />
      <Drawer.Screen name="socialMedia" component={SocialMediaContainer} />
      <Drawer.Screen name="collections" component={MyCollection} />
      <Drawer.Screen name="Table" component={TableContainer} />
      <Drawer.Screen name="PublicProfile" component={PublicProfile} />
      <Drawer.Screen name="contents" component={ContentsContainer} />
      <Drawer.Screen name ="privacy" component ={PrivacyContainer}/>
      <Drawer.Screen name = "Terms" component = {TermsContainer}/>
    </Drawer.Navigator>
  );
};

const RootNavigation = () => {
  return (
    <NavigationContainer>
      <BASE_STACK.Navigator
        screenOptions={{
          headerShown: false,
          contentStyle: {
            backgroundColor: 'white',
          },
        }}
        initialRouteName={'Splash'}>
        <BASE_STACK.Screen name="Splash" component={Splash} />
        <BASE_STACK.Screen name="login" component={Login} />
        <BASE_STACK.Screen name="forgot" component={ForgotPassword} />
        <Drawer.Screen name="StdReg" component={StudentRegister} />
        <Drawer.Screen name="ParentReg" component={ParentRegister} />
        {/* <BASE_STACK.Screen name="Notifications" component={Notifications} />
        <BASE_STACK.Screen name="profileList" component={ProfileList} />
        <BASE_STACK.Screen name="profile" component={profileContainer} /> */}
        <BASE_STACK.Screen name="Home" component={homeDrawer} />
        {/* <BASE_STACK.Screen name="news" component={newsFeedContainer} />
        <BASE_STACK.Screen name="StdReg" component={StudentRegister} />
        <BASE_STACK.Screen name="ParentReg" component={ParentRegister} />
        <BASE_STACK.Screen name="changePass" component={ChangePassword} /> */}
        {/* <BASE_STACK.Screen
          name="notificationSetting"
          component={NotificationSettingContainer}
        />
        <BASE_STACK.Screen name="schoolId" component={SchoolidContainer} />
        <BASE_STACK.Screen
          name="socialMedia"
          component={SocialMediaContainer}
        />
        <BASE_STACK.Screen name="collections" component={MyCollection} /> */}
      </BASE_STACK.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigation;
