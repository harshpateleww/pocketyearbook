import React from 'react';
import {StatusBar, View} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import PYBNavBar from '../Components/PYBNavBar';

const BaseContainer = props => {
  return (
    <SafeAreaView
      style={{flex: 1, backgroundColor: 'white'}}
      forceInset={{top: 'always'}}>
      <StatusBar backgroundColor={'white'} barStyle="dark-content" />

      <PYBNavBar
        title={props.title}
        leftComponent={props.leftComponent}
        navigation={props.navigation}
        textStyle={props.textStyle}
        rightComponent={props.rightComponent}
        onGoBack={props.onGoBack}
      />
      <View style={{flex:1}}>
      {props.children}
      </View>
    </SafeAreaView>
  );
};

export default BaseContainer;
