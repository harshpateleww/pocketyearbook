import React, {useState} from 'react';
import {SafeAreaView, Image, Switch, View, Text} from 'react-native';
import {Assets} from '../Assets/Assets';
import PYBNavBar from '../Components/PYBNavBar';
import {CNFonts} from '../Utils/Fonts';
import BaseContainer from './BaseContainer';

const SocialMediaContainer = ({navigation}) => {
  const [isEnabled, setIsEnabled] = useState(false);
  const [isEnabled1, setIsEnabled1] = useState(false);
  const [isEnabled2, setIsEnabled2] = useState(false);
  const [isEnabled3, setIsEnabled3] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);
  const toggleSwitch1 = () => setIsEnabled1(previousState => !previousState);
  const toggleSwitch2 = () => setIsEnabled2(previousState => !previousState);
  const toggleSwitch3 = () => setIsEnabled3(previousState => !previousState);

  return (
    <BaseContainer
      textStyle={{color: 'black'}}
      onGoBack={() => navigation.goBack()}
      title="Connect with me">
      <View
        style={{
          paddingHorizontal: 20,
          flexDirection: 'row',
          marginVertical: 20,
          justifyContent: 'space-between',
        }}>
        <View style={{flexDirection: 'row'}}>
          <Image source={Assets.fb}></Image>
          <Text
            style={{
              color: 'grey',
              fontFamily: CNFonts.Roboto_Medium,
              fontSize: 16,
              marginHorizontal: 20,
            }}>
            www.facebook.com
          </Text>
        </View>
        <View>
          <Switch
            trackColor={{false: '#0000', true: '#B51613'}}
            thumbColor={isEnabled ? '#f4f3f4' : '#f4f3f4'}
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
        </View>
      </View>
      <View
        style={{
          paddingHorizontal: 20,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <View style={{flexDirection: 'row'}}>
          <Image source={Assets.insta}></Image>
          <Text
            style={{
              color: 'grey',
              fontFamily: CNFonts.Roboto_Medium,
              fontSize: 16,
              marginHorizontal: 20,
            }}>
            www.instagram.com
          </Text>
        </View>
        <View>
          <Switch
            trackColor={{false: '#0000', true: '#B51613'}}
            thumbColor={isEnabled1 ? '#f4f3f4' : '#f4f3f4'}
            onValueChange={toggleSwitch1}
            value={isEnabled1}
          />
        </View>
      </View>
      <View
        style={{
          paddingHorizontal: 20,
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginVertical: 20,
        }}>
        <View style={{flexDirection: 'row'}}>
          <Image source={Assets.linkdin}></Image>
          <Text
            style={{
              color: 'grey',
              fontFamily: CNFonts.Roboto_Medium,
              fontSize: 16,
              marginHorizontal: 20,
            }}>
            www.linkedin.com
          </Text>
        </View>
        <View>
          <Switch
            trackColor={{false: '#0000', true: '#B51613'}}
            thumbColor={isEnabled2 ? '#f4f3f4' : '#f4f3f4'}
            onValueChange={toggleSwitch2}
            value={isEnabled2}
          />
        </View>
      </View>
      <View
        style={{
          paddingHorizontal: 20,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <View style={{flexDirection: 'row'}}>
          <Image source={Assets.twitter}></Image>
          <Text
            style={{
              color: 'grey',
              fontFamily: CNFonts.Roboto_Medium,
              fontSize: 16,
              marginHorizontal: 20,
            }}>
            www.twitter.com
          </Text>
        </View>
        <View>
          <Switch
            trackColor={{false: '#0000', true: '#B51613'}}
            thumbColor={isEnabled3 ? '#f4f3f4' : '#f4f3f4'}
            onValueChange={toggleSwitch3}
            value={isEnabled3}
          />
        </View>
      </View>
    </BaseContainer>
  );
};
export default SocialMediaContainer;
