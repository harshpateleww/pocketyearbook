import React from 'react';
import {Pressable} from 'react-native';
import {Image} from 'react-native';
import {ImageBackground} from 'react-native';
import {ScrollView} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {TextInput} from 'react-native';
import {SafeAreaView} from 'react-native';
import {View, Text} from 'react-native';
import {Icon} from 'react-native-elements/dist/icons/Icon';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import { CNFonts } from '../Utils/Fonts';
import BaseContainer from './BaseContainer';
import ProfileList from './ProfileList';

const SingleYearBook = ({props, navigation}) => {
  return (
    <BaseContainer
      leftComponent={
        <View
          style={{
            flexDirection: 'row',
            flex: 1,
            justifyContent: 'space-around',
          }}>
          <Pressable onPress={() => navigation.navigate('contents')}>
            <Image source={require('../Assets/Images/menuBar.png')} />
          </Pressable>
          <Pressable onPress={() => navigation.navigate('Notifications')}>
            <Image source={require('../Assets/Images/notification.png')} />
          </Pressable>
        </View>
      }
      textStyle={{color: 'black'}}
      title={`Excellent WebWorld
      (2021-2022)`}
      rightComponent={
        <View
          style={{
            flexDirection: 'row',
            flex: 1,
            justifyContent: 'space-around',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => navigation.navigate('PublicProfile')}>
            <Image source={require('../Assets/Images/people1.png')} />
          </TouchableOpacity>
          <Pressable onPress={() => navigation.navigate('collections')}>
            <Image source={require('../Assets/Images/stackBook.png')} />
          </Pressable>
        </View>
      }>
      <View
        style={{
          flexDirection: 'row',
          borderBottomWidth: 0.7,
          borderBottomColor: 'grey',
          paddingBottom: 5,
          marginHorizontal: 20,
          marginBottom: 5,
          paddingVertical: 15,
        }}>
        <Icon size={22} name="search" type="material-icons" color={'grey'} />
        <TextInput
          placeholder={'Search'}
          style={{
            borderBottomColor: 'grey',
            fontSize: 18,
            paddingLeft: 10,
          }}
        />
      </View>
      <ScrollView style={{flex: 1}}>
        <View style={{alignItems: 'center'}}>
          <Text style={{fontSize: 19, marginVertical: 10, fontFamily:CNFonts.Roboto_Medium}}>
            POCKET YEARBOOK
          </Text>
          <Image
          style={{width: widthPercentageToDP("100%"), height: widthPercentageToDP("33%")}}
          source={require('../Assets/Images/groupPeople.png')} />
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 10,
            marginVertical: 5,
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image 
            source={require('../Assets/Images/people.png')} />
            <Text style={{marginLeft: 5}}>0</Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image source={require('../Assets/Images/heart.png')} />
            <Text style={{marginLeft: 5}}>0</Text>
          </View>
        </View>
        <Text style={{paddingHorizontal: 10, fontSize: 15}}>
          Lorem excepteur cupidatat occaecat eu do minim excepteur incididunt.
          Irure cupidatat Lorem duis cillum adipisicing ullamco commodo
          consectetur ullamco ea. Dolore minim Lorem esse consequat.
          {/* Excepteur Lorem sit ullamco do laborum occaecat excepteur pariatur
          exercitation ut ullamco est aliquip cillum. Proident cupidatat labore
          Lorem veniam exercitation sint ex amet. */}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginVertical: 5,
          }}>
          <Image 
          style={{width: widthPercentageToDP("50%") - 10, height: widthPercentageToDP("20%")}}
          source={require('../Assets/Images/library2.png')} />
          {/* <Image
            source={require('../Assets/Images/mute.png')}
            style={{
              position: 'absolute',
              left: 160,
              top: 10,
              zIndex: 1,
              borderWidth:5
            }}
          />
          <Image
            source={require('../Assets/Images/enlarge.png')}
            style={{
              position: 'absolute',
              left: 160,
              top: 100,
              zIndex: 1,
              borderWidth:5
            }}
          /> */}
          <View>
            <Image 
            style={{
              width: widthPercentageToDP("50%") - 10,
              height: widthPercentageToDP("20%")
            }}
            source={require('../Assets/Images/graduateGirl.png')} />
            <View
              style={{
                flexDirection: 'row',
                marginHorizontal: 10,
                marginVertical: 5,
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image source={require('../Assets/Images/people.png')} />
                <Text style={{marginLeft: 5}}>0</Text>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image source={require('../Assets/Images/heart.png')} />
                <Text style={{marginLeft: 5}}>0</Text>
              </View>
            </View>
          </View>
        </View>
        <Text style={{paddingHorizontal: 10, fontSize: 15}}>
          Lorem excepteur cupidatat occaecat eu do minim excepteur incididunt.
          Irure cupidatat Lorem duis cillum adipisicing ullamco commodo
          consectetur ullamco ea. Dolore minim Lorem esse consequat.
          {/* Excepteur Lorem sit ullamco do laborum occaecat excepteur pariatur
          exercitation ut ullamco est aliquip cillum. Proident cupidatat labore
          Lorem veniam exercitation sint ex amet. */}
        </Text>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text
            style={{paddingHorizontal: 10, fontSize: 13, marginVertical: 10}}>
            Link
          </Text>
          <Text
            style={{
              color: '#0095F4',
            }}>{`https://www.excellentwebworld.com/`}</Text>
        </View>

        <View
          style={{
            width: '100%',
            height: 10,
            backgroundColor: 'gray',
            opacity: 0.4,
            shadowOpacity: 0.4,
            shadowRadius: 2,
            shadowOffset: {height: 1, width: 1},
          }}
        />

        <View style={{alignItems: 'center'}}>
          <Text style={{fontSize: 19, marginVertical: 10, fontWeight: '400'}}>
            EXCELLENT WEBWORLD
          </Text>
          <Image 
          style={{width: widthPercentageToDP("100%"), height: widthPercentageToDP("33%")}}
          source={require('../Assets/Images/groupPeople.png')} />
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 10,
            marginVertical: 5,
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image source={require('../Assets/Images/people.png')} />
            <Text style={{marginLeft: 5}}>0</Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image source={require('../Assets/Images/heart.png')} />
            <Text style={{marginLeft: 5}}>0</Text>
          </View>
        </View>
        <Text style={{paddingHorizontal: 10, fontSize: 15}}>
          Lorem excepteur cupidatat occaecat eu do minim excepteur incididunt.
          Irure cupidatat Lorem duis cillum adipisicing ullamco commodo
          consectetur ullamco ea. Dolore minim Lorem esse consequat.
          {/* Excepteur Lorem sit ullamco do laborum occaecat excepteur pariatur
          exercitation ut ullamco est aliquip cillum. Proident cupidatat labore
          Lorem veniam exercitation sint ex amet. */}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginVertical: 5,
          }}>
          <Image 
          style={{width: widthPercentageToDP("50%") - 10, height: widthPercentageToDP("20%")}}
          source={require('../Assets/Images/library2.png')} />
          {/* <Image
            source={require('../Assets/Images/mute.png')}
            style={{
              position: 'absolute',
              left: 160,
              top: 10,
              zIndex: 1,
            }}
          />
          <Image
            source={require('../Assets/Images/enlarge.png')}
            style={{
              position: 'absolute',
              left: 160,
              top: 100,
              zIndex: 1,
            }}
          /> */}
          <View>
            <Image 
            style={{width: widthPercentageToDP("50%") - 10, height: widthPercentageToDP("20%")}}
            source={require('../Assets/Images/graduateGirl.png')} />
            <View
              style={{
                flexDirection: 'row',
                marginHorizontal: 10,
                marginVertical: 5,
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image source={require('../Assets/Images/people.png')} />
                <Text style={{marginLeft: 5}}>0</Text>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image source={require('../Assets/Images/heart.png')} />
                <Text style={{marginLeft: 5}}>0</Text>
              </View>
            </View>
          </View>
        </View>
        <Text style={{paddingHorizontal: 10, fontSize: 15}}>
          Lorem excepteur cupidatat occaecat eu do minim excepteur incididunt.
          Irure cupidatat Lorem duis cillum adipisicing ullamco commodo
          consectetur ullamco ea. Dolore minim Lorem esse consequat.
          {/* Excepteur Lorem sit ullamco do laborum occaecat excepteur pariatur
          exercitation ut ullamco est aliquip cillum. Proident cupidatat labore
          Lorem veniam exercitation sint ex amet. */}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            paddingBottom: 20,
          }}>
          <Text
            style={{paddingHorizontal: 10, fontSize: 13, marginVertical: 10}}>
            Link
          </Text>
          <Text
            style={{
              color: '#0095F4',
            }}>{`https://www.excellentwebworld.com/`}</Text>
        </View>
        <ProfileList />
      </ScrollView>
    </BaseContainer>
  );
};

export default SingleYearBook;
