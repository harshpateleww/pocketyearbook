import React from 'react';
import {View, Text, Image, FlatList} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {Assets} from '../Assets/Assets';
import BaseContainer from '../Container/BaseContainer';
import {SafeAreaView} from 'react-native-safe-area-context';
import PYBNavBar from '../Components/PYBNavBar';
import {widthPercentageToDP, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {CNFonts} from '../Utils/Fonts';

const Notifications = ({navigation}) => {
  const arrayData = [
    {
      name: 'Scrambled Eggs',
      description: 'Name invited you to write in yearbook! ',
      image: Assets.profile_pic,
      date: 'January 16,2021',
      time: '12:00 pm',
    },
    {
      name: 'Sandwich',
      description: 'Name invited you to write in yearbook! ',
      image: Assets.pic2,
      date: 'February 16,2021',
      time: '01:00 pm ',
    },
    {
      name: 'Omlette',
      description: 'Name invited you to write in yearbook! ',
      image: Assets.pic3,
      date: 'March 16,2021',
      time: '02:00 pm',
    },
    {
      name: 'Scrambled Eggs',
      description: 'Name invited you to write in yearbook! ',
      image: Assets.pic4,
      date: 'April 16,2021',
      time: '03 :00 pm',
    },
    {
      name: 'Scrambled Eggs',
      description: 'Name invited you to write in yearbook! ',
      image: Assets.profile_pic,
      date: 'January 16,2021',
      time: '12:00 pm',
    },
    {
      name: 'Sandwich',
      description: 'Name invited you to write in yearbook! ',
      image: Assets.pic2,
      date: 'February 16,2021',
      time: '01:00 pm ',
    },
    {
      name: 'Omlette',
      description: 'Name invited you to write in yearbook! ',
      image: Assets.pic3,
      date: 'March 16,2021',
      time: '02:00 pm',
    },
    {
      name: 'Scrambled Eggs',
      description: 'Name invited you to write in yearbook! ',
      image: Assets.pic4,
      date: 'April 16,2021',
      time: '03 :00 pm',
    },
    {
      name: 'Scrambled Eggs',
      description: 'Name invited you to write in yearbook! ',
      image: Assets.profile_pic,
      date: 'January 16,2021',
      time: '12:00 pm',
    },
    {
      name: 'Sandwich',
      description: 'Name invited you to write in yearbook! ',
      image: Assets.pic2,
      date: 'February 16,2021',
      time: '01:00 pm ',
    },
    {
      name: 'Omlette',
      description: 'Name invited you to write in yearbook! ',
      image: Assets.pic3,
      date: 'March 16,2021',
      time: '02:00 pm',
    },
    {
      name: 'Scrambled Eggs',
      description: 'name invited you to write in yearbook! ',
      image: Assets.pic4,
      date: 'April 16,2021',
      time: '03 :00 pm',
    },
    {
      name: 'Scrambled Eggs',
      description: 'Name invited you to write in yearbook! ',
      image: Assets.profile_pic,
      date: 'January 16,2021',
      time: '12:00 pm',
    },
    {
      name: 'Sandwich',
      description: 'Name invited you to write in yearbook  ',
      image: Assets.pic2,
      date: 'February 16,2021',
      time: '01:00 pm ',
    },
    {
      name: 'Omlette',
      description: 'Name invited you to write in yearbook! ',
      image: Assets.pic3,
      date: 'March 16,2021',
      time: '02:00 pm',
    },
    {
      name: 'Scrambled Eggs',
      description: 'Name invited you to write in yearbook! ',
      image: Assets.pic4,
      date: 'April 16,2021',
      time: '03 :00 pm',
    },
  ];

  const renderNotificationList = item => {
    return (
      <View
        style={{
          marginTop: 15,
          marginLeft: wp('4%'),
          flexDirection: 'row',
          marginRight: wp('8%'),
        }}>
        <Image source={item.image}></Image>
        <View style={{marginLeft: wp('3%')}}>
          <Text
            style={{
              fontSize: 14,
              color: 'black',
              fontFamily: CNFonts.Roboto_Medium,
            }}>
            {item.name}
          </Text>
          <Text style={{fontSize: 12, fontFamily: CNFonts.Roboto_Regular}}>
            {item.description}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 20,
              flex: 1,
              width: widthPercentageToDP('70%'),
              justifyContent: 'space-between',
            }}>
            <Text
              style={{
                fontSize: 11,
                color: 'grey',
                fontFamily: CNFonts.Roboto_Regular,
              }}>
              {item.date}
            </Text>
            <Text
              style={{
                fontSize: 11,
                color: 'grey',
                fontFamily: CNFonts.Roboto_Regular,
              }}>
              {item.time}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  return (
    <BaseContainer
      onGoBack={() => navigation.navigate('SingleYearBook')}
      title="Notifications"
      textStyle={{
        color: 'black',
        fontFamily: CNFonts.Roboto_Bold,
        fontSize: 14,
      }}>
      <FlatList
        style={{flex: 1}}
        data={arrayData}
        renderItem={({item}) => renderNotificationList(item)}
      />
    </BaseContainer>
  );
};
export default Notifications;
