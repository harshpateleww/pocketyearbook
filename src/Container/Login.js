import React from 'react';
import {View, Text, Image, ScrollView, Platform} from 'react-native';
import {Assets} from '../Assets/Assets';
import PYBButton from '../Components/PYBButton';
import PYBInput from '../Components/PYBInput';
import {Pressable} from 'react-native';
import {CNFonts} from '../Utils/Fonts';
import {Dimensions} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import { heightPercentageToDP } from 'react-native-responsive-screen';
// import {KeyboardAvoidingView} from 'react-native';

const Login = ({props, navigation}) => {
  return (
    <KeyboardAwareScrollView style={{flex: 1}}
    
    >
      <View style={{flex: 1}}>
        <Image
          style={{
            width: Dimensions.get('window').width,
            height: heightPercentageToDP("30%"),
          }}
          resizeMode={'cover'}
          source={Assets.file}
        />
        <View
          style={{
            alignSelf: 'center',

            backgroundColor: 'white',
            marginTop: -80,
            paddingHorizontal: 30,
            paddingVertical: 40,
            borderRadius: 80,
          }}>
          <Image
            style={
              {
                // top: '60%',
                // position: 'absolute',
              }
            }
            source={Assets.iconName}
          />
        </View>
        <Image
          resizeMode={'contain'}
          style={{alignSelf: 'center', marginTop: -25}}
          source={Assets.appName}></Image>
      </View>
      <View style={{paddingHorizontal: 20}}>
        <PYBInput placeholder={'Email'} />
        <PYBInput placeholder={'Password'} secureTextEntry />
        <Pressable onPress={() => navigation.navigate('forgot')}>
          <Text
            style={{
              alignSelf: 'flex-end',
              fontSize: 14,
              marginVertical: 25,
              fontFamily: CNFonts.Roboto_Medium,
              color: 'black',
            }}>
            {'Forgot password?'}
          </Text>
        </Pressable>
        <PYBButton
          style={{
            backgroundColor: '#B51613',
            fontSize: 14,
            padding: 15,
            alignItems: 'center',
          }}
          title={'LOG IN'}
          onPress={() => navigation.navigate('Home')}
          textStyle={{color: 'white', fontFamily: CNFonts.Roboto_Bold}}
        />
        <View
          style={{
            marginVertical: 30,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              height: 1,
              width: '40%',
              alignSelf: 'center',
              borderColor: '#888888',
              borderWidth: 0.3,
            }}></View>
          <Text
            style={{
              fontSize: 14,
              color: 'black',
              fontFamily: CNFonts.Roboto_Bold,
            }}>
            OR
          </Text>
          <View
            style={{
              width: '40%',
              height: 1,
              alignSelf: 'center',
              borderColor: '#888888',
              borderWidth: 0.3,
            }}></View>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <PYBButton
            onPress={() => navigation.navigate('StdReg')}
            style={{
              backgroundColor: 'black',
              fontSize: 14,
              alignItems: 'center',
              paddingVertical: 20,
              width: '45%',
            }}
            textStyle={{color: 'white', fontFamily: CNFonts.Roboto_Medium}}
            title={'Register As Student'}
          />
          <PYBButton
            onPress={() => navigation.navigate('ParentReg')}
            style={{
              backgroundColor: 'black',
              fontSize: 14,
              alignItems: 'center',
              paddingVertical: 20,
              width: '45%',
            }}
            textStyle={{color: 'white', fontFamily: CNFonts.Roboto_Medium}}
            title={'Register As Parent'}
          />
        </View>
      </View>
      <View style={{flex: 1}}>
        <Image
          style={{
            marginTop: '20%',
            // flex: 1,
            width: Dimensions.get('window').width,
            height:Platform.isPad ? heightPercentageToDP("31%") : heightPercentageToDP("23%")
          }}
          resizeMode='contain'
          source={Assets.backImage}/>
      </View>
    </KeyboardAwareScrollView>
  );
};
export default Login;
