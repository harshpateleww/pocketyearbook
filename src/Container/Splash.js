import React, {useEffect, useState} from 'react';
import {StatusBar, View} from 'react-native';
import {Image} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import TouchID from 'react-native-touch-id';
import {Assets} from '../Assets/Assets';

const Splash = props => {
  useEffect(() => {
    setTimeout(() => {
      props.navigation.navigate('login');
      SplashScreen.hide();
    }, 3000);
  });

  return (
    <>
      {/* <Image
        style={{width: '100%', height: '100%'}}
        resizeMode="cover"
        source={Assets.splash}
      /> */}
      <View style={{flex: 1}}></View>
    </>
  );

  // useEffect(() => {
  // setTimeout(() => {
  //   props.navigation.navigate('login');
  // }, 3000);
  // }, []);
  // const optionalConfigObject = {
  //   title: 'Authentication Required', // Android
  //   imageColor: '#e00606', // Android
  //   imageErrorColor: '#ff0000', // Android
  //   sensorDescription: 'Touch sensor', // Android
  //   sensorErrorDescription: 'Failed', // Android
  //   cancelText: 'Cancel', // Android
  //   fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
  //   unifiedErrors: false, // use unified error messages (default false)
  //   passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
  // };

  // const _pressHandler = () => {
  //   TouchID.authenticate(
  //     'to demo this react-native component',
  //     optionalConfigObject,
  //   )
  //     .then(success => {
  //       Alert.alert('Authenticated Successfully');
  //     })
  //     .catch(error => {
  //       Alert.alert('Authentication Failed');
  //     });
  // };

  // const clickHandler = () => {
  //   TouchID.isSupported()
  //     .then(biometryType => {
  //       // Success code
  //       if (biometryType === 'FaceID') {
  //         console.log('FaceID is supported.');
  //       } else if (biometryType === 'TouchID') {
  //         console.log('TouchID is supported.');
  //       } else if (biometryType === true) {
  //         // Touch ID is supported on Android
  //       }
  //     })
  //     .catch(error => {
  //       // Failure code if the user's device does not have touchID or faceID enabled
  //       console.log(error);
  //     });
  // };

  // return (
  //   <>
  //     <StatusBar backgroundColor="blue" barStyle="dark-content" />
  //     <SafeAreaView
  //       forceInset={{top: 'always'}}
  //       style={{backgroundColor: 'blue'}}
  //     />

  //     {/* <PYBButton title={'HELLO'} />
  //     <PYBInput placeholder={'Email'} />
  //     <PYBInput placeholder={'Password'} secureTextEntry /> */}
  //   </>
  // );
};
export default Splash;
