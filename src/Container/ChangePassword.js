import React from 'react';
import {ScrollView} from 'react-native';
import {Pressable} from 'react-native';
import {SafeAreaView} from 'react-native';
import {View, Text, Image} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {Assets} from '../Assets/Assets';
import PYBButton from '../Components/PYBButton';
import PYBInput from '../Components/PYBInput';
import PYBNavBar from '../Components/PYBNavBar';
import {CNFonts} from '../Utils/Fonts';
import BaseContainer from './BaseContainer';
// import {CNFonts} from '../Utils/Fonts';

const ChangePassword = ({navigation}) => {
  return (
    <BaseContainer
      textStyle={{
        color: 'black',
        fontFamily: CNFonts.Roboto_Bold,
        fontSize: 14,
      }}
      onGoBack={() => navigation.goBack()}
      title="Change Password">
      <View
        style={{
          paddingHorizontal: widthPercentageToDP('5%'),
          marginVertical: heightPercentageToDP('5%'),
        }}>
        <PYBInput placeholder={'Current password'} />
        <PYBInput placeholder={'New password'} />
        <PYBInput placeholder={'Confirm password'} />
      </View>
      <View
        style={{
          paddingHorizontal: widthPercentageToDP('7%'),
          justifyContent: 'flex-end',
          marginVertical: heightPercentageToDP('10%'),
        }}>
        <PYBButton
          style={{
            backgroundColor: '#B51613',
            fontSize: 14,
            padding: 15,
            alignItems: 'center',
          }}
          title={'SAVE'}
          textStyle={{color: 'white'}}
        />
      </View>
    </BaseContainer>
  );
};
export default ChangePassword;
