import React from 'react';
import {Dimensions} from 'react-native';
import {SafeAreaView} from 'react-native';
import {View, Text} from 'react-native';
import PYBButton from '../Components/PYBButton';
import PYBInput from '../Components/PYBInput';
import PYBNavBar from '../Components/PYBNavBar';
import {CNFonts} from '../Utils/Fonts';
import BaseContainer from './BaseContainer';

const SchoolidContainer = ({navigation}) => {
  return (
    <BaseContainer
      textStyle={{
        color: 'black',
        fontFamily: CNFonts.Roboto_Bold,
        fontSize: 14,
      }}
      onGoBack={() => navigation.goBack()}
      title="My School ID">
      <View
        style={{
          padding: 30,
          backgroundColor: '#D3D3D3	',
        }}>
        <PYBInput placeholder={'Enter Your ID'} />
        <PYBButton
          style={{
            backgroundColor: '#B51613',
            fontSize: 14,
            alignItems: 'center',
            paddingVertical: 10,
            marginVertical: 40,
          }}
          textStyle={{color: 'white', fontFamily: CNFonts.Roboto_Medium}}
          title={'SAVE'}
        />
      </View>
    </BaseContainer>
  );
};
export default SchoolidContainer;
