import React, {useState} from 'react';
import {SafeAreaView} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {View, Text, Switch} from 'react-native';
import PYBNavBar from '../Components/PYBNavBar';
import {CNFonts} from '../Utils/Fonts';
import BaseContainer from './BaseContainer';

const NotificationSettingContainer = ({navigation}) => {
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);

  return (
  
     <BaseContainer
      textStyle={{
        color: 'black',
        fontFamily: CNFonts.Roboto_Bold,
        fontSize: 14,
      }}
      onGoBack={() => navigation.goBack()}
      title="Notification Setting">

      <View style={{paddingHorizontal: 20, marginVertical: 30}}>
        <Text
          style={{
            color: 'black',
            fontFamily: CNFonts.Roboto_Bold,
            fontSize: 16,
          }}>
          ALLOW NOTIFICATIONS
        </Text>
        <View
          style={{
            flexDirection: 'row',
            marginVertical: 20,
            justifyContent: 'space-between',
          }}>
          <Text
            style={{
              color: 'grey',
              fontFamily: CNFonts.Roboto_Medium,
              fontSize: 16,
            }}>
            Push Notifications
          </Text>
          <Switch
            style={{justifyContent: 'flex-end'}}
            trackColor={{false: '#767577', true: '#B51613'}}
            thumbColor={isEnabled ? '#f4f3f4' : '#f4f3f4'}
            ios_backgroundColor="#3e3e3e"
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text
            style={{
              color: 'grey',
              fontFamily: CNFonts.Roboto_Medium,
              fontSize: 16,
            }}>
            E -mail Notifications
          </Text>
          <Switch
            style={{justifyContent: 'flex-end'}}
            trackColor={{false: '#767577', true: '#B51613'}}
            thumbColor={isEnabled ? '#f4f3f4' : '#f4f3f4'}
            ios_backgroundColor="#3e3e3e"
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
        </View>

        <Text
          style={{
            color: 'grey',
            fontFamily: CNFonts.Roboto_Medium,
            fontSize: 16,
            marginVertical: 20,
          }}>
          Change E -mail
        </Text>
      </View>
    </BaseContainer>
  );
};
export default NotificationSettingContainer;
