import React from 'react';
import {
  View,
  Text,
  ScrollView,
  FlatList,
  Image,
  TextInput,
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import {Assets} from '../Assets/Assets';
import PYBInput from '../Components/PYBInput';
import {Icon} from 'react-native-elements';
import {Dimensions} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

const ProfileList = () => {
  const profileList = [
    {
      name: 'Jahna Sadeghi',
      image: Assets.man1,
    },
    {
      name: 'Jake Heimovics',
      image: Assets.man2,
    },
    {
      name: 'Ria Tomlin',
      image: Assets.man3,
    },
    {
      name: 'Jahna Sadeghi',
      image: Assets.man1,
    },
    {
      name: 'Jake Heimovics',
      image: Assets.man2,
    },
    {
      name: 'Ria Tomlin',
      image: Assets.man3,
    },
    {
      name: 'Jahna Sadeghi',
      image: Assets.man1,
    },
    {
      name: 'Jake Heimovics',
      image: Assets.man2,
    },
    {
      name: 'Ria Tomlin',
      image: Assets.man3,
    },
    {
      name: 'Jahna Sadeghi',
      image: Assets.man1,
    },
    {
      name: 'Jake Heimovics',
      image: Assets.man2,
    },
    {
      name: 'Ria Tomlin',
      image: Assets.man3,
    },
    {
      name: 'Jahna Sadeghi',
      image: Assets.man1,
    },
    {
      name: 'Jake Heimovics',
      image: Assets.man2,
    },
    {
      name: 'Ria Tomlin',
      image: Assets.man3,
    },
    {
      name: 'Jahna Sadeghi',
      image: Assets.man1,
    },
    {
      name: 'Jake Heimovics',
      image: Assets.man2,
    },
    {
      name: 'Ria Tomlin',
      image: Assets.man3,
    },
    {
      name: 'Jahna Sadeghi',
      image: Assets.man1,
    },
    {
      name: 'Jake Heimovics',
      image: Assets.man2,
    },
    {
      name: 'Ria Tomlin',
      image: Assets.man3,
    },
    {
      name: 'Jahna Sadeghi',
      image: Assets.man1,
    },
    {
      name: 'Jake Heimovics',
      image: Assets.man2,
    },
    {
      name: 'Ria Tomlin',
      image: Assets.man3,
    },
    {
      name: 'Jahna Sadeghi',
      image: Assets.man1,
    },
    {
      name: 'Jake Heimovics',
      image: Assets.man2,
    },
    {
      name: 'Ria Tomlin',
      image: Assets.man3,
    },
    {
      name: 'Jahna Sadeghi',
      image: Assets.man1,
    },
    {
      name: 'Jake Heimovics',
      image: Assets.man2,
    },
    {
      name: 'Ria Tomlin',
      image: Assets.man3,
    },
    {
      name: 'Jahna Sadeghi',
      image: Assets.man1,
    },
    {
      name: 'Jake Heimovics',
      image: Assets.man2,
    },
    {
      name: 'Ria Tomlin',
      image: Assets.man3,
    },
    {
      name: 'Jahna Sadeghi',
      image: Assets.man1,
    },
    {
      name: 'Jake Heimovics',
      image: Assets.man2,
    },
    {
      name: 'Ria Tomlin',
      image: Assets.man3,
    },
    {
      name: 'Jahna Sadeghi',
      image: Assets.man1,
    },
    {
      name: 'Jake Heimovics',
      image: Assets.man2,
    },
    {
      name: 'Ria Tomlin',
      image: Assets.man3,
    },
  ];

  const renderProfileList = item => {
    return (
      <View
        style={{
          paddingHorizontal: 15,
          alignItems: 'center',
          flex: 1,
        }}>
        <Image 
        style={{width: widthPercentageToDP("33%") - 15, height: widthPercentageToDP("40%")
      }} 
        
        source={item.image}></Image>

        <Text
          style={{
            fontSize: widthPercentageToDP("3%"),
            marginVertical: 10,
          }}>
          {item.name}
        </Text>
      </View>
    );
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{flex: 1}}>
        <FlatList
          numColumns={3}
          data={profileList}
          contentContainerStyle={{
            justifyContent: 'space-between',
            marginTop: 5,
          }}
          renderItem={({item}) => renderProfileList(item)}
        />
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  textInputStyle: {
    borderBottomColor: 'grey',
    fontSize: 18,
    paddingLeft: 10,
    // fontFamily: CNFonts.Roboto_Bold,
  },
  iconStyle: {
    paddingLeft: 20,
  },
  searchIconStyle: {
    padding: 3,
  },
});

export default ProfileList;
