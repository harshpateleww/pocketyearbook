import React from 'react';
import {StatusBar} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {
  View,
  Image,
  Text,
  ScrollView,
  FlatList,
  Dimensions,
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Assets} from '../Assets/Assets';
import PYBNavBar from '../Components/PYBNavBar';
import {CNFonts} from '../Utils/Fonts';

const HomeContainer = props => {
  const arrayData = [
    {
      name: 'Thank you to our PTO',
      description:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, ',
      image: Assets.profile_pic,
      date: 'January 16,2021',
      time: ' 12:00 pm',
    },
    {
      name: 'Thank you to our PTO',
      description:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, ',
      image: Assets.profile_pic,
      date: 'January 16,2021',
      time: ' 12:00 pm',
    },
    {
      name: 'Thank you to our PTO',
      description:
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, ',
      image: Assets.profile_pic,
      date: 'January 16,2021',
      time: ' 12:00 pm',
    },
  ];

  const renderhomePageList = item => {
    return (
      <>
        <View style={{flex: 1, marginTop: 10}}>
          <View
            style={{
              paddingHorizontal: 20,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Text style={{fontFamily: CNFonts.Roboto_Regular, fontSize: 13}}>
              {item.date}
            </Text>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{marginTop: heightPercentageToDP('0.1%')}}
                source={Assets.clock}></Image>
              <Text style={{fontFamily: CNFonts.Roboto_Regular, fontSize: 13}}>
                {item.time}
              </Text>
            </View>
          </View>
          <Text
            style={{
              marginVertical: 10,
              fontSize: 15,
              fontFamily: CNFonts.Roboto_Medium,
              paddingHorizontal: 20,
              color: 'black',
            }}>
            {item.name}
          </Text>
          <Image
            style={{
              width: widthPercentageToDP('100%'),
              height: widthPercentageToDP('33%'),
            }}
            resizeMode={'cover'}
            source={Assets.fimage}></Image>
          <Text
            style={{
              paddingHorizontal: 20,
              marginVertical: 20,
              fontSize: 15,
              fontFamily: CNFonts.Roboto_Regular,
              color: 'black',
            }}>
            {item.description}
          </Text>
        </View>
      </>
    );
  };

  const renderSeparator = () => {
    return (
      <View
        style={{
          height: 10,
          width: '100%',
          backgroundColor: '#E7E7E7',
        }}
      />
    );
  };

  return (
    <SafeAreaView style={{backgroundColor: 'white', flex: 1}}>
      <StatusBar backgroundColor={'white'} barStyle="dark-content" />
      <View
        style={{
          height: 30,
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingHorizontal: 15,
        }}>
        <TouchableOpacity onPress={() => props.navigation.toggleDrawer()}>
          <Image style={{marginTop: 7}} source={Assets.setting}></Image>
        </TouchableOpacity>
        <Image source={Assets.pocket}></Image>
        <TouchableOpacity
          onPress={() => props.navigation.navigate('collections')}>
          <Image style={{marginTop: 7}} source={Assets.book}></Image>
        </TouchableOpacity>
      </View>
      <ScrollView contentContainerStyle={{flex: 1}}>
        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 10,
            paddingVertical: 20,
          }}>
          <Image
            style={{
              height: widthPercentageToDP('12%'),
              width: widthPercentageToDP('10%'),
            }}
            source={Assets.man1}></Image>
          <View style={{flex: 1, marginLeft: 20}}>
            <Text
              style={{
                fontSize: 18,
                fontFamily: CNFonts.Roboto_Medium,
                color: 'black',
              }}>
              Lauren German
            </Text>
            <Text
              style={{
                fontSize: 12,
                color: 'grey',
                marginVertical: 10,
                fontFamily: CNFonts.Roboto_Regular,
              }}>
              ID : 1118101
            </Text>
            <View
              style={{
                flexDirection: 'row',

                // width: widthPercentageToDP('66%'),
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  fontSize: 14,
                  color: 'black',
                  fontFamily: CNFonts.Roboto_Regular,
                }}>
                Excellent Webworld
              </Text>
              <Image
                style={{
                  marginTop: heightPercentageToDP('0.5%'),
                  marginRight: widthPercentageToDP('1%'),
                }}
                source={Assets.msg}></Image>
            </View>
          </View>
        </View>
        <View
          style={{
            width: '100%',
            borderWidth: 4,
            borderColor: '#E7E7E7',
          }}></View>
        <FlatList
          data={arrayData}
          renderItem={({item}) => renderhomePageList(item)}
          ItemSeparatorComponent={renderSeparator}
        />
      </ScrollView>
    </SafeAreaView>
  );
};
export default HomeContainer;
