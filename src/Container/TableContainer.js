import React from 'react';
import {SafeAreaView} from 'react-native';
import {StyleSheet, Text, View} from 'react-native';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import PYBNavBar from '../Components/PYBNavBar';

const TableContainer = () => {
  return (
    <>
      <SafeAreaView />
      <PYBNavBar
        textStyle={{
          // fontFamily: CNFonts.Roboto_Bold,
          fontSize: 14,
        }}
        title={'Excellent Webworld'}
      />
      <View
        style={{
          alignItems: 'center',
          paddingVertical: heightPercentageToDP('2%'),
          borderBottomWidth: 2,
          borderBottomColor: 'grey',
        }}>
        <Text style={{fontSize: 16}}>TABLE OF CONTENTS</Text>
      </View>
    </>
  );
};

export default TableContainer;
