import React from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  Platform,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Assets} from '../Assets/Assets';
import PYBButton from '../Components/PYBButton';
import PYBInput from '../Components/PYBInput';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  heightPercentageToDP,
} from 'react-native-responsive-screen';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const ForgotPassword = ({props, navigation}) => {
  return (
    <KeyboardAwareScrollView style={{flex: 1}}>
      <View style={{flex: 1}}>
        <Image
          style={{
            width: Dimensions.get('window').width,
            height: 250,
          }}
          resizeMode={'cover'}
          source={Assets.file}
        />
        <View
          style={{
            alignSelf: 'center',

            backgroundColor: 'white',
            marginTop: -80,
            paddingHorizontal: 30,
            paddingVertical: 40,
            borderRadius: 80,
          }}>
          <Image
            style={
              {
                // top: '60%',
                // position: 'absolute',
              }
            }
            source={Assets.iconName}
          />
        </View>
        <Image
          resizeMode={'contain'}
          style={{alignSelf: 'center'}}
          source={Assets.appName}></Image>
        <TouchableOpacity
          style={{position: 'absolute', top: '5%'}}
          onPress={() => navigation.navigate('login')}>
          <Image
            style={{marginLeft: 20}}
            resizeMode={'contain'}
            source={Assets.back}></Image>
        </TouchableOpacity>
        <View style={{paddingHorizontal: 20, marginTop: 40}}>
          <PYBInput placeholder={'Forgot Email'} />
          <PYBButton
            style={{
              backgroundColor: '#B51613',
              fontSize: 14,
              padding: 15,
              marginVertical: 30,
              alignItems: 'center',
            }}
            title={'Forgot Email'}
            textStyle={{color: 'white'}}
            onPress={() => navigation.navigate('login')}
          />
        </View>
        {/* <Image
          style={{
            width: '100%',
            marginTop: hp('26%'),
          }}
          resizeMode={'cover'}
          source={Assets.backImage}></Image> */}
          <Image
          style={{
            marginTop: '20%',
            // flex: 1,
            width: Dimensions.get('window').width,
            height:Platform.isPad ? heightPercentageToDP("31%") : heightPercentageToDP("23%")
          }}
          resizeMode='contain'
          source={Assets.backImage}/>
      </View>
    </KeyboardAwareScrollView>
  );
};
export default ForgotPassword;
