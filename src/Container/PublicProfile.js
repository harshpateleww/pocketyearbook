import React from 'react';
import {TextInput} from 'react-native';
import {ScrollView} from 'react-native';
import {View, Text, Image} from 'react-native';
import {Icon} from 'react-native-elements';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Assets} from '../Assets/Assets';
import PYBButton from '../Components/PYBButton';
import {CNFonts} from '../Utils/Fonts';
import BaseContainer from './BaseContainer';

const publicProfile = ({navigation}) => {
  const imgArr = [
    require('../Assets/Images/graduateGirl.png'),
    require('../Assets/Images/graduateGirl.png'),
    require('../Assets/Images/graduateGirl.png'),
    require('../Assets/Images/graduateGirl.png'),
    require('../Assets/Images/graduateGirl.png'),
  ];
  return (
    <BaseContainer
      textStyle={{
        color: 'black',
        fontFamily: CNFonts.Roboto_Bold,
        fontSize: 14,
      }}
      onGoBack={() => navigation.navigate('SingleYearBook')}
      title="My Profile">
      <ScrollView
        style={{
          flex: 1,
          marginTop: 20,
        }}>
        <View style={{flexDirection: 'row', marginHorizontal: 20}}>
          <Image source={Assets.man4}></Image>
          <View style={{marginLeft: 20}}>
            <Text style={{fontSize: 17, color: 'black'}}>Tom James</Text>
            <Text style={{fontSize: 13, fontWeight: '100'}}>12th Grade</Text>
            <Text style={{fontSize: 12, marginTop: 10, color: 'black'}}>
              Connect with me:
            </Text>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 12,
              }}>
              <Image source={Assets.fb}></Image>
              <Image
                style={{marginHorizontal: 5}}
                source={Assets.linkdin}></Image>
              <Image source={Assets.twitter}></Image>
              <Image
                style={{marginHorizontal: 5}}
                source={Assets.insta}></Image>
            </View>
          </View>
        </View>
        <View
          style={{
            height: 1,
            backgroundColor: '#999',
            width: '100%',
            marginVertical: 20,
          }}
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 10,
          }}>
          <View>
            <Text style={{fontWeight: '400', color: 'gray', marginBottom: 4}}>
              sports/clubs
            </Text>
            <Text>Boys Varsity Soccer </Text>
          </View>
          <Image source={require('../Assets/Images/noun-edit-1610855.png')} />
        </View>
        <View
          style={{
            width: '100%',
            height: 10,
            backgroundColor: 'gray',
            opacity: 0.4,
            marginVertical: 20,
            shadowOpacity: 0.4,
            shadowRadius: 2,
            shadowOffset: {height: 1, width: 1},
          }}
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 10,
          }}>
          <View>
            <Text style={{fontWeight: '400', color: 'gray', marginBottom: 4}}>
              Photos/Videos
            </Text>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={{color: '#0095F4'}}>{`View `}</Text>
            <Image source={require('../Assets/Images/viewAr.png')} />
          </View>
        </View>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          horizontal
          style={{
            flexDirection: 'row',
            paddingVertical: 10,
            marginHorizontal: 10,
            maxHeight: 90,
          }}>
          <Image
            source={require('../Assets/Images/graduateGirl.png')}
            style={{height: 75, width: 75, marginRight: 5}}
          />
          <Image
            source={require('../Assets/Images/library.png')}
            style={{height: 75, width: 75, marginRight: 5}}
          />
          <Image
            source={require('../Assets/Images/profile_pic.png')}
            style={{height: 75, width: 75, marginRight: 5}}
          />
          <Image
            source={require('../Assets/Images/fullImage.png')}
            style={{height: 75, width: 75, marginRight: 5}}
          />
          <Image
            source={require('../Assets/Images/graduateGirl.png')}
            style={{height: 75, width: 75, marginRight: 5}}
          />
        </ScrollView>
        <View
          style={{
            width: '100%',
            height: 10,
            backgroundColor: 'gray',
            opacity: 0.4,
            marginVertical: 20,
            shadowOpacity: 0.4,
            shadowRadius: 2,
            shadowOffset: {height: 1, width: 1},
          }}
        />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              flexDirection: 'row',
              borderBottomWidth: 0.7,
              borderBottomColor: 'grey',
              paddingBottom: 5,
              marginLeft: 20,
              marginBottom: 5,
              paddingVertical: 15,
              width: '50%',
            }}>
            <Icon
              size={22}
              name="search"
              type="material-icons"
              color={'grey'}
            />
            <TextInput
              placeholder={'Search'}
              style={{
                borderBottomColor: 'grey',
                fontSize: 18,
                paddingLeft: 10,
              }}
            />
          </View>
          <PYBButton
            textStyle={{color: 'red'}}
            style={{
              backgroundColor: 'white',
              paddingVertical: 10,
              flex: 1,
              marginHorizontal: 10,
              marginRight: 30,
              shadowColor: '#000',
              shadowOffset: {width: 0, height: 2},
              shadowOpacity: 0.5,
              shadowRadius: 2,
              elevation: 2,
              alignItems: 'center',
            }}
            title="Invite"
          />
        </View>
        <View
          style={{
            width: '100%',
            height: 10,
            backgroundColor: 'gray',
            opacity: 0.4,
            marginVertical: 15,
            shadowOpacity: 0.4,
            shadowRadius: 2,
            shadowOffset: {height: 1, width: 1},
          }}
        />
        <Text
          style={{
            fontSize: 14,
            fontWeight: 'bold',
            alignSelf: 'center',
            color: 'black',
          }}>
          FRIENDS
        </Text>
        <View
          style={{
            height: 0.5,
            marginVertical: 10,
            marginBottom: 30,
            opacity: 0.5,
            flexDirection: 'row',
            backgroundColor: '#707070',
          }}
        />
        <View
          style={{
            marginHorizontal: 20,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View style={{flexDirection: 'row'}}>
            <Image source={Assets.man4} style={{height: 80, width: 80}}></Image>
            <View style={{marginLeft: 20}}>
              <Text style={{fontSize: 17}}>Tom James</Text>
              <Text style={{fontSize: 13, fontWeight: '100'}}>12th Grade</Text>
            </View>
          </View>
          <Text
            style={{opacity: 0.5}}>{`April 08,2019 \n         7:31 AM `}</Text>
        </View>
        <View style={{marginHorizontal: 10}}>
          <Text style={{fontSize: 13, marginVertical: 10}}>
            Hey Anthony! All of your hard work paid off this year. I remember
            you told me you would do whatever it takes to make the varsity team,
            and now you are a starter! I'm proud of you, keep up the great wor..
          </Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={{color: '#0095F4'}}>{`Read More `}</Text>
            <Image
              source={require('../Assets/Images/viewAr.png')}
              style={{transform: [{rotate: '90deg'}], marginLeft: 5}}
            />
          </View>
        </View>
        <View
          style={{
            height: 0.5,
            marginVertical: 10,
            marginBottom: 30,
            width: '95%',
            alignSelf: 'center',
            opacity: 0.5,
            flexDirection: 'row',
            backgroundColor: '#707070',
          }}
        />

        {/* <View
          style={{
            shadowColor: '#000',
            shadowOffset: {width: 0, height: 2},
            shadowOpacity: 0.5,
            shadowRadius: 2,
            paddingVertical: 30,
            flexDirection: 'row',
            borderBottomWidth: 0.7,
            borderBottomColor: 'grey',
            justifyContent: 'space-evenly',
          }}>
          <PYBButton
            style={{
              backgroundColor: 'white',
              paddingVertical: 10,
              padding: 15,
            }}
            textStyle={{color: 'red'}}
            title={'Write in Yearbook'}></PYBButton>
          <PYBButton
            style={{
              backgroundColor: 'black',
              paddingVertical: 10,
              padding: 20,
            }}
            textStyle={{color: 'white'}}
            title={'Send Invitation'}></PYBButton>
        </View> */}
      </ScrollView>
    </BaseContainer>
  );
};
export default publicProfile;
