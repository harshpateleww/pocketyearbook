import React from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  FlatList,
  Image,
  Text,
  ScrollView,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Icon} from 'react-native-elements';
import {Assets} from '../Assets/Assets';
import BaseContainer from './BaseContainer';
import {CNFonts} from '../Utils/Fonts';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

const newsFeedContainer = () => {
  const arrayData = [
    {
      name: 'Scrambled Eggs',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudL orem ipsum dolor sit amet, consectetur adipiscing ...',
      image: Assets.profile_pic,
      date: 'january 16,2021',
      time: '12:00 pm',
      count: 0,
      img2: Assets.people,
      Link: 'https://www.excellentwebworld.com/',
    },
    {
      name: 'Scrambled Eggs',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudL orem ipsum dolor sit amet, consectetur adipiscing ...',
      image: Assets.profile_pic,
      date: 'january 16,2021',
      time: '12:00 pm',
      count: 0,
      img2: Assets.people,
      Link: 'https://www.excellentwebworld.com/',
    },
    {
      name: 'Scrambled Eggs',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrudL orem ipsum dolor sit amet, consectetur adipiscing ...',
      image: Assets.profile_pic,
      date: 'january 16,2021',
      time: '12:00 pm',
      count: 0,
      img2: Assets.people,
      Link: 'https://www.excellentwebworld.com/',
    },
    // {
    //   name: 'Sandwich',
    //   description: 'name invited you to write in yearbook! ',
    //   image: Assets.pic2,
    //   date: 'february 16,2021',
    //   time: '01:00 pm ',
    // },
    // {
    //   name: 'Omlette',
    //   description: 'name invited you to write in yearbook! ',
    //   image: Assets.pic3,
    //   date: 'march 16,2021',
    //   time: '02:00 pm',
    // },
  ];
  const renderFeedList = item => {
    return (
      <>
        <View style={{paddingHorizontal: 10, flex: 1}}>
          <Text
            style={{
              fontSize: 19,
              marginTop: 10,
              marginBottom: 15,
              alignSelf: 'center',
            }}>
            {'Pocket Yearbook'}
          </Text>
          <Image
            style={{width: 400, height: 150}}
            resizeMode={'contain'}
            source={Assets.fimage}></Image>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{flexDirection: 'row', marginTop: 5}}>
              <Image
                resizeMode={'contain'}
                style={{marginTop: 3}}
                source={Assets.people}></Image>
              <Text
                style={{
                  marginHorizontal: 7,
                }}>
                0
              </Text>
            </View>
            <View style={{flexDirection: 'row', marginTop: 5}}>
              <Image
                resizeMode={'contain'}
                style={{marginTop: 3}}
                source={Assets.heart}></Image>
              <Text
                style={{
                  marginHorizontal: 7,
                }}>
                {item.count}
              </Text>
            </View>
          </View>
          <Text
            style={{
              marginVertical: 10,
              fontFamily: CNFonts.Roboto_Regular,
              fontSize: 14,
              color: 'black',
            }}>
            {item.description}
          </Text>
          <Text style={{fontSize: 13}}>{'Link : ' + item.Link}</Text>
        </View>
      </>
    );
  };
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <View
        style={{
          height: 30,
          borderBottomWidth: 1,
          borderBottomColor: 'grey',
          flexDirection: 'row',
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            marginLeft: widthPercentageToDP('5%'),
          }}>
          <Image source={Assets.menu}></Image>
          <Image
            style={{marginHorizontal: widthPercentageToDP('4%')}}
            source={Assets.notification}></Image>
        </View>
        <View style={{flex: 2, marginLeft: widthPercentageToDP('4%')}}>
          <Text style={{fontSize: 14}}>Excellent Webworld</Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-end',
            paddingRight: widthPercentageToDP('5%'),
          }}>
          <Image
            style={{
              height: 25,
              width: 25,
              marginHorizontal: widthPercentageToDP('4%'),
            }}
            resizeMode={'contain'}
            source={Assets.people1}></Image>
          <Image style={{paddingTop: 10}} source={Assets.book}></Image>
        </View>
      </View>
      <FlatList
        nestedScrollEnabled
        data={arrayData}
        renderItem={({item}) => renderFeedList(item)}
      />
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  textInputStyle: {
    borderBottomColor: 'grey',
    fontSize: 18,
    paddingLeft: 10,
    // fontFamily: CNFonts.Roboto_Bold,
  },
  iconStyle: {
    paddingLeft: 20,
  },
  searchIconStyle: {
    padding: 3,
  },
});
export default newsFeedContainer;
