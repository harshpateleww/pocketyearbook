import React from 'react';
import {View, Text, Image} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Assets} from '../Assets/Assets';
import PYBButton from '../Components/PYBButton';
import PYBNavBar from '../Components/PYBNavBar';
import {CNFonts} from '../Utils/Fonts';

const profileContainer = props => {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <PYBNavBar
        textStyle={{
          fontSize: 14,
          color: 'black',
          fontFamily: CNFonts.Roboto_Bold,
          borderBottomColor: 'grey',
        }}
        title={'Profile'}
      />
      <View
        style={{
          flex: 1,
          marginTop: 20,
        }}>
        <View style={{flexDirection: 'row', paddingHorizontal: 15}}>
          <Image source={Assets.man4}></Image>
          <View style={{marginLeft: 20}}>
            <Text
              style={{
                fontSize: 17,
                fontFamily: CNFonts.Roboto_Medium,
                color: 'black',
              }}>
              Tom James
            </Text>
            <Text style={{fontSize: 13, fontFamily: CNFonts.Roboto_Medium}}>
              12th Grade
            </Text>
            <Text
              style={{
                fontSize: 12,
                marginTop: 10,
                fontFamily: CNFonts.Roboto_Regular,
              }}>
              Connect with me:
            </Text>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 12,
              }}>
              <Image source={Assets.fb}></Image>
              <Image
                style={{marginHorizontal: 5}}
                source={Assets.linkdin}></Image>
              <Image source={Assets.twitter}></Image>
              <Image
                style={{marginHorizontal: 5}}
                source={Assets.insta}></Image>
            </View>
          </View>
        </View>
        <View
          style={{
            paddingVertical: 30,
            flexDirection: 'row',
            borderBottomWidth: 0.7,
            borderBottomColor: 'grey',
          }}>
          <PYBButton
            style={{
              width: 140,
              backgroundColor: 'white',
              marginLeft: 30,
              alignItems: 'center',
              paddingVertical: 15,
              shadowColor: '#000',
              shadowOffset: {width: 0, height: 2},
              shadowOpacity: 0.5,
              shadowRadius: 2,
              elevation: 2,
            }}
            textStyle={{
              color: 'red',
              fontSize: 14,
              fontFamily: CNFonts.Roboto_Bold,
            }}
            title={'Write in Yearbook'}></PYBButton>
          <PYBButton
            style={{
              width: 140,
              backgroundColor: 'black',
              paddingVertical: 10,
              marginLeft: 10,
              alignItems: 'center',
              paddingVertical: 15,
            }}
            textStyle={{
              color: 'white',
              fontSize: 14,
              fontFamily: CNFonts.Roboto_Bold,
            }}
            title={'Send Invitation'}></PYBButton>
        </View>
      </View>
    </SafeAreaView>
  );
};
export default profileContainer;
