import {useLinkProps} from '@react-navigation/native';
import React from 'react';
import {SafeAreaView} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {View, Text, Image, Dimensions} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {Assets} from '../Assets/Assets';

const CustomDrawer = ({props, navigation}) => {
  return (
    <SafeAreaView style={{backgroundColor: 'white', flex: 1}}>
      <View style={{borderBottomWidth: 15, borderBottomColor: 'grey', flex: 1}}>
        <View
          style={{
            alignItems: 'center',
            paddingVertical: 15,
            borderBottomWidth: 1,
          }}>
          <Text style={{color: 'black'}}>Settings</Text>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate('Home')}
          style={{
            paddingVertical: 15,
            flexDirection: 'row',
          }}>
          <Image
            style={{
              marginHorizontal: 20,
              marginTop: heightPercentageToDP('0.5%'),
            }}
            source={Assets.dashboard}></Image>
          <Text style={{color: 'black'}}>News Feed</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => navigation.navigate('notificationSetting')}
          style={{
            paddingVertical: 15,
            flexDirection: 'row',
          }}>
          <Image
            style={{
              marginHorizontal: 20,
              marginTop: heightPercentageToDP('0.3%'),
            }}
            source={Assets.notification}></Image>
          <Text style={{color: 'black'}}>Notifications</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('changePass')}
          style={{
            paddingVertical: 15,
            flexDirection: 'row',
          }}>
          <Image
            style={{
              marginHorizontal: 20,
              marginTop: heightPercentageToDP('0.3%'),
            }}
            source={Assets.padlock}></Image>
          <Text
            style={{
              color: 'black',
              paddingHorizontal: widthPercentageToDP('0.3%'),
            }}>
            Change Password
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('socialMedia')}
          style={{
            paddingVertical: 15,
            flexDirection: 'row',
          }}>
          <Image
            style={{
              marginHorizontal: 20,
              marginTop: heightPercentageToDP('0.3%'),
            }}
            source={Assets.social}></Image>
          <Text style={{color: 'black'}}>Social Media</Text>
        </TouchableOpacity>
        {/* <TouchableOpacity
          onPress={() => navigation.navigate('socialMedia')}
          style={{
            paddingVertical: 15,
            flexDirection: 'row',
          }}>
          <Image
            style={{
              marginHorizontal: 20,
              marginTop: heightPercentageToDP('0.6%'),
            }}
            source={Assets.msg}></Image>
          <Text style={{color: 'black'}}>Contact Yearbook Team</Text>
        </TouchableOpacity> */}
        <TouchableOpacity
          onPress={() => navigation.navigate('schoolId')}
          style={{
            paddingVertical: 15,
            flexDirection: 'row',
          }}>
          <Image
            style={{
              marginHorizontal: 20,
              marginTop: heightPercentageToDP('0.5%'),
            }}
            source={Assets.idcard}></Image>
          <Text style={{color: 'black'}}>My School ID</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('Terms')}
          style={{
            paddingVertical: 15,
            flexDirection: 'row',
          }}>
          <Image style={{marginHorizontal: 20}} source={Assets.terms}></Image>
          <Text style={{color: 'black'}}>Terms/Conditions</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('privacy')}
          style={{
            paddingVertical: 15,
            flexDirection: 'row',
          }}>
          <Image
            style={{
              marginHorizontal: 20,
              marginTop: heightPercentageToDP('0.5%'),
            }}
            source={Assets.privacy}></Image>
          <Text style={{color: 'black'}}>privacy</Text>
        </TouchableOpacity>
        <View style={{height: heightPercentageToDP('24%')}}></View>
      </View>
      <TouchableOpacity
        onPress={() => navigation.navigate('login')}
        style={{
          alignItems: 'center',
          paddingVertical: 15,
          borderBottomWidth: 2,
        }}>
        <Text style={{color: 'black'}}>Logout</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default CustomDrawer;
