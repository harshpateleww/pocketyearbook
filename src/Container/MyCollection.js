import React from 'react';
import {TouchableOpacity} from 'react-native';
import {StyleSheet, Text, View, Image, FlatList} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Assets} from '../Assets/Assets';
import PYBNavBar from '../Components/PYBNavBar';
import {CNFonts} from '../Utils/Fonts';

const MyCollection = ({props, navigation}) => {
  const arrayData = [
    {
      name: 'Add YearBook',
      image: Assets.yearbook,
    },
    {
      name: '2021-2022',
      image: Assets.static,
    },
  ];

  const renderFeedList = item => {
    return (
      <TouchableOpacity
        onPress={() => navigation.navigate('SingleYearBook')}
        style={{
          marginHorizontal: widthPercentageToDP('5%'),
        }}>
        <Image
          style={{height: 200, width: 160}}
          resizeMode={'contain'}
          source={item.image}></Image>
        <Text
          style={{
            alignSelf: 'center',
            paddingVertical: heightPercentageToDP('1%'),
          }}>
          {item.name}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={{flex: 1,backgroundColor:'white' }}>
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            height: 50,
            borderBottomWidth: 1,
            borderBottomColor: 'grey',
          }}>
          <View style={{flex: 1}}>
            <TouchableOpacity onPress={() => navigation.navigate('SingleYearBook')}>
              <Image
                style={{marginLeft: widthPercentageToDP('5%')}}
                source={Assets.dashboard}></Image>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 2,
              flexDirection: 'row',
            }}>
            <Text
              style={{
                textAlign: 'center',
                fontSize: 14,
                fontFamily: CNFonts.Roboto_Bold,
                color: 'black',
                paddingLeft: widthPercentageToDP('4%'),
              }}>
              My Collection
            </Text>
          </View>
        </View>
        <FlatList
          contentContainerStyle={{marginTop: heightPercentageToDP('3%')}}
          numColumns={2}
          data={arrayData}
          renderItem={({item}) => renderFeedList(item)}
        />
      </View>
    </SafeAreaView>
  );
};
export default MyCollection;
