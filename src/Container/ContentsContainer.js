import {Text, SafeAreaView, View, Pressable, Image} from 'react-native';
import React, {useState} from 'react';
import {Icon} from 'react-native-elements';
import BaseContainer from './BaseContainer';
import {TouchableOpacity} from 'react-native';
import {CNFonts} from '../Utils/Fonts';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {Assets} from '../Assets/Assets';

const ContentsContainer = ({navigation}) => {
  const [pressed, setPressed] = useState(false);
  // const [pressed1, setPressed1] = useState(false)
  return (
    <BaseContainer
      leftComponent={<View style={{flex: 1}} />}
      textStyle={{color: 'black', alignItems: 'center'}}
      onGoBack={() => navigation.goBack()}
      title={`Excellent WebWorld
      (2021-2022)`}
      rightComponent={
        <View
          style={{
            flexDirection: 'row',
            flex: 1,
            justifyContent: 'flex-end',
          }}>
          <Pressable onPress={() => navigation.navigate('SingleYearBook')}>
            <Image style={{height: 15, width: 15}} source={Assets.cancel} />
          </Pressable>
        </View>
      }>


      <View
        style={{
          borderWidth: 1,
          alignItems: 'center',
          borderColor: 'grey',
          paddingVertical: heightPercentageToDP('1%'),
        }}>
        <Text
          style={{
            color: 'black',
            fontSize: 16,
            fontFamily: CNFonts.Roboto_Bold,
          }}>
          Table Of Contents
        </Text>
      </View>
      <View
        style={{
          borderWidth: 1,
          alignItems: 'center',
          borderColor: 'grey',
          paddingVertical: heightPercentageToDP('1%'),
        }}>
        <Text
          style={{
            color: 'black',
            fontSize: 16,
          }}>
Cover        </Text>
      </View>
      <Pressable
        onPress={() => setPressed(!pressed)}
        style={{
          backgroundColor: 'white',

          flexDirection: 'row',
          justifyContent: 'space-between',
          // paddingLeft: widthPercentageToDP('40%'),
          borderBottomColor: 'grey',
          paddingVertical:10,
          borderBottomWidth: 1,
        }}>
        <Text
          style={{
            flex:1,
            color: 'black',
            fontSize: 14,
            textAlign:'center',
            fontFamily: CNFonts.Roboto_Regular

          }}>
          {'School Spirit'}
        </Text>
        <Icon  name="arrow-drop-down" color="black" />
      </Pressable>
      {/* <Pressable
        onPress={() => setPressed1(!pressed1)}
        style={{
          backgroundColor: 'white',

          flexDirection: 'row',
          justifyContent: 'space-between',
          // paddingLeft: widthPercentageToDP('40%'),
          borderBottomColor: 'grey',
          paddingVertical:10,
          borderBottomWidth: 1,
        }}>
        <Text
          style={{
            flex:1,
            color: 'black',
            fontSize: 14,
            textAlign:'center',
            fontFamily: CNFonts.Roboto_Regular

          }}>
          {'Sports'}
        </Text>
        <Icon  name="arrow-drop-down" color="black" />
      </Pressable> */}
      {pressed && (
        <View
          style={{
            backgroundColor: '#fff',
            alignItems: 'center',
          }}>
          <Text
            style={{
              color: 'black',
              paddingVertical: heightPercentageToDP('2%'),
              borderBottomColor: 'grey',
            }}>
            Grades
          </Text>
          <Text
            style={{
              color: 'black',
              paddingVertical: heightPercentageToDP('2%'),
              borderBottomColor: 'grey',
            }}>
            Clubs
          </Text>
          <Text
            style={{
              color: 'black',
              paddingVertical: heightPercentageToDP('2%'),
              borderBottomColor: 'grey',
            }}>
            Events
          </Text>
        </View>
      )}
    </BaseContainer>
  );
};

export default ContentsContainer;
