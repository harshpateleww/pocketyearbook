import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  Platform,
} from 'react-native';
import {Assets} from '../Assets/Assets';
import PYBButton from '../Components/PYBButton';
import PYBInput from '../Components/PYBInput';
import {Pressable} from 'react-native';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const ParentRegister = ({props, navigation}) => {
  const [isChild, setIsChild] = useState(false);

  return (
    <KeyboardAwareScrollView style={{flex: 1}}>
      <View style={{flex: 1}}>
        <View>
          <Image
            style={{
              width: Dimensions.get('window').width,
              height: 250,
            }}
            resizeMode={'cover'}
            source={Assets.file}
          />
          <View
            style={{
              alignSelf: 'center',

              backgroundColor: 'white',
              marginTop: -80,
              paddingHorizontal: 30,
              paddingVertical: 40,
              borderRadius: 80,
            }}>
            <Image
              style={
                {
                  // top: '60%',
                  // position: 'absolute',
                }
              }
              source={Assets.iconName}
            />
          </View>

          <Image
            resizeMode={'contain'}
            style={{alignSelf: 'center'}}
            source={Assets.appName}></Image>
          <TouchableOpacity
            style={{position: 'absolute', top: heightPercentageToDP('5%')}}
            onPress={() => navigation.navigate('login')}>
            <Image
              style={{marginLeft: 20}}
              resizeMode={'contain'}
              source={Assets.back}></Image>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{paddingHorizontal: 20}}>
        <PYBInput placeholder={'Parent Full Name*'} />
        <PYBInput placeholder={'Parent E-mail*'} />
        <Pressable onPress={() => setIsChild(!isChild)}>
          <View
            style={{
              height: 60,
              width: 60,
              borderRadius: 60,
              backgroundColor: '#D3D3D3',
              marginVertical: 20,
            }}>
            <Image
              style={{
                padding: 30,
                height: 50,
                width: 50,
              }}
              resizeMode="contain"
              source={Assets.hat}></Image>
            <Text style={{alignSelf: 'center', color: 'grey'}}>Child</Text>
          </View>
        </Pressable>
        {isChild == true ? (
          <View>
            <PYBInput placeholder={'Student Full Name*'} />
            <PYBInput placeholder={'Student ID*'} />
            <PYBInput placeholder={'Grade Level*'} />
            <PYBInput placeholder={'City*'} />
            <PYBInput placeholder={'State*'} />
            <PYBInput placeholder={'School name*'} />
          </View>
        ) : null}
        <PYBButton
          style={{
            backgroundColor: '#B51613',
            fontSize: 14,
            padding: 15,
            marginTop: 30,
            alignItems: 'center',
          }}
          title={'SUBMIT'}
          textStyle={{color: 'white'}}
        />
        <View
          style={{
            marginVertical: 30,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}></View>
      </View>
      {/* <Image
        style={{
          width: '100%',
          alignItems: 'flex-end',
          marginTop: '28%',
        }}
        resizeMode={'cover'}
        source={Assets.backImage}></Image> */}
        <Image
          style={{
            marginTop: '20%',
            // flex: 1,
            width: Dimensions.get('window').width,
            height:Platform.isPad ? heightPercentageToDP("31%") : heightPercentageToDP("23%")
          }}
          resizeMode='contain'
          source={Assets.backImage}/>
    </KeyboardAwareScrollView>
  );
};
export default ParentRegister;
