export const CNFonts = {
  Roboto_Bold: 'Roboto-Bold',
  Roboto_Light: 'Roboto-Light',
  Roboto_Medium: 'Roboto-Medium',
  Roboto_Regular: 'Roboto-Regular',
  Roboto_SemiBold: 'Roboto--SemiBold',
};
