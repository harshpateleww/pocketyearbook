import React, {useState} from 'react';
import {Icon} from 'react-native-elements';
import {TouchableOpacity, TextInput, StyleSheet, View} from 'react-native';
import {PYBColors} from '../Utils/PYBColors';
const PYBInput = props => {
  const [isVisibility, setIsVisibility] = useState(false);

  const onVisibilityHandler = () => {
    setIsVisibility(!isVisibility);
  };
  return (
    <View style={[styles.containerStyle, props.containerStyle]}>
      <TextInput
        placeholder={props.placeholder}
        placeholderTextColor={'#707070'}
        style={[styles.textInputStyle, props.textInputStyle]}
        secureTextEntry={isVisibility}
      />
      {props.secureTextEntry ? (
        <Icon
          type={'material-icons'}
          name={isVisibility ? 'visibility-off' : 'visibility'}
          onPress={() => setIsVisibility(!isVisibility)}
        />
      ) : null}
    </View>
  );
};
const styles = StyleSheet.create({
  containerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 4,
    borderBottomWidth: 0.6,
    borderBottomColor: '#707070',
  },
  textInputStyle: {
    // fontFamily: CNFonts.Roboto_Bold,
    flex: 1,
    paddingVertical: 5,
    // fontFamily: CNFonts.Roboto_Regular,
    color: 'black',
    borderBottomColor: '#DCDCDC',
    fontSize: 16,
  },
  iconStyle: {
    paddingLeft: 20,
  },
});

export default PYBInput;
