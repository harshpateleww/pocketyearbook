import React from 'react';
import {Pressable} from 'react-native';
import {Image, StyleSheet, Text, View} from 'react-native';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import {Assets} from '../Assets/Assets';

const PYBNavBar = (props) => {
  return (
    <>
      <View style={[styles.container, props.containerStyle]}>
        {props.leftComponent ? (
          props.leftComponent
        ) : (
          <Pressable onPress={props.onGoBack} style={{flex: 1}}>
            <Image style={styles.leftButton} source={Assets.back} />
          </Pressable>
        )}
        {props.title ? (
          <View
            style={
              ([styles.titleComponent],
              {paddingVertical: props.title ? 8.5 : 0})
            }>
            <Text style={[props.textStyle, styles.titleComponentText]}>
              {props.title}
            </Text>
          </View>
        ) : props.image ? (
          <View style={styles.titleComponent}>
            <Image source={props.image} />
          </View>
        ) : null}
        {props.rightComponent ? (
          props.rightComponent
        ) : (
          <View
            style={{
              flex: 1,
              //  borderWidth: 1
            }}
          />
        )}
      </View>
      <View
        style={{
          width: '100%',
          height: 1,
          backgroundColor: 'gray',
          opacity: 0.5,
          marginTop: 10,
        }}
      />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 15,
  },
  titleComponent: {
    flex: 2,
    flexDirection: 'row',
    // borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleComponentText: {
    textAlign: 'center',
    fontWeight: '700',
  },
  rightComponent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
});

export default PYBNavBar;
