import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

const PYBButton = props => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={props.onPress}
      style={[styles.btnStyle, props.style]}>
      <Text style={[styles.BtnText, props.textStyle]}>{props.title}</Text>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  btnStyle: {
    borderRadius: 7,
  },
});
export default PYBButton;
