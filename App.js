import React from 'react';
import RootNavigation from './src/RootNavigation';
import 'react-native-gesture-handler';

const App = () => {
  return (
    <>
      <RootNavigation />
    </>
  );
};

export default App;
